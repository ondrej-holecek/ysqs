const AWS = require('aws-sdk')
const express = require('express');
const app = express();
const sqs = new AWS.SQS({ region: "us-east-1" });


class YSQS {
    constructor(sqs_url, params) {
        this.next_id = 1;
        this.sqs_url = sqs_url;
        this.params = params;
        this.handles = {};
        this.sqs = new AWS.SQS({ region: "us-east-1" });
    }

    rm(id, cb) {
        let h = this.handles[id];
        if (!h) {
            cb([404, "Not Found"]);
            return;
        }

        let params = {
            QueueUrl: this.sqs_url,
            ReceiptHandle: h.handle
        }

        clearTimeout(h.timeout);
        delete this.handles[id];
        sqs.deleteMessage(params, (err, data) => {
            cb(err, data);
        });
    }

    get(id, cb) {
        let handles = this.handles;
        let h = handles[id];
        if (h) {
            cb(null, h.data);
            return;
        }

        cb([404, 'Not Found']);
    }

    pop(id, cb) {
        let self = this;
        let handles = this.handles;
        let h = handles[id];
        let sqs_url = this.sqs_url;
        if (h) {
            cb(null, h.data);
            return;
        }

        let params = {
            QueueUrl: sqs_url,
            AttributeNames: [ "All" ],
            MaxNumberOfMessages: 1,
            WaitTimeSeconds: 5,
            VisibilityTimeout: this.params.visibility_timeout
        }

        sqs.receiveMessage(params, (err, j) => {
            if (err) {
                console.log(err);
                cb([500, 'Internal server error']);
                return;
            }

            if (!j || !j.Messages || j.Messages.length == 0) {
                cb([404, 'Not Found']);
                return;
            }


            let m = j.Messages[0]
            let handle = m.ReceiptHandle;
            let data = m.Body;
            let stamp = this.next_id;
            this.next_id += 1;

            let set_timeout = function() {
                let h = handles[id];
                if (!h) return;
                if (h.stamp != stamp) return;
                h.timeout = setTimeout(() => {
                    console.log('timeout handle = ' + h.handle);
                    let params = {
                        QueueUrl: sqs_url,
                        ReceiptHandle: h.handle,
                        VisibilityTimeout: self.params.visibility_timeout
                    }

                    sqs.changeMessageVisibility(params, (err, data) => {
                        if (err) console.log("sqs.changeMessageVisibility failed!", err);
                        set_timeout();
                    })
                }, self.params.refresh_time * 1000);
            }


            handles[id] = {
                handle: handle,
                data: data,
                stamp: stamp
            }

            set_timeout();

            cb(null, data);
        })

    }
}


function register_queue(app, name, uri) {
    let ysqs_params = {
        visibility_timeout: 10,
        refresh_time: 5
    };

    let ysqs = new YSQS(uri, ysqs_params);
    let router = express.Router();

    function handle(req, res, err, data) {
            if (err) {
                try {
                    res.status(err[0]).send(err[1]);
                } catch(err2) {
                    console.log(err2);
                    res.status(500).send('Internal server error');
                }
            } else {
                res.send(data);
            }
    }

    router.get('/:id', (req, res) => {
        ysqs.get(req.params.id, (err, data) => {
            handle(req, res, err, data);
        });
    });

    router.put('/:id', (req, res) => {
        ysqs.pop(req.params.id, (err, data) => {
            handle(req, res, err, data);
        });
    });

    router.delete('/:id', (req, res) => {
        ysqs.rm(req.params.id, (err, data) => {
            if (!err) {
                handle(req, res, err, data);
            } else {
                handle(req, res, err, 'ok');
            }
        });
    });


    app.use('/' + name, router)
}


register_queue(app, 'smrt28', 'https://sqs.us-east-1.amazonaws.com/436820408400/smrt28');
app.listen(3000, () => console.log('Example app listening on port 3000!'));
